package com.advice.automation.automationreport;


import javax.persistence.*;
import java.sql.Date;
import java.sql.Time;

@Entity
@Table(name = "reports")
public class CucumberReport {
    private Integer id;
    private String testId;
    private String scenarioName;
    private String componentName;
    private Time executionTime;
    private Long duration;
    private String statusTest;
    private String imageBase;
    private String executionTag;
    private int executionNumber;
    private Date dateOfExecution;

    public CucumberReport(){
    }

    public CucumberReport(Integer id, String testId, String scenarioName, String componentName, Time executionTime, Long duration, String statusTest, String imageBase, String executionTag, int executionNumber, Date dateOfExecution) {
        super();
        this.id = id;
        this.testId = testId;
        this.scenarioName = scenarioName;
        this.componentName = componentName;
        this.executionTime = executionTime;
        this.duration = duration;
        this.statusTest = statusTest;
        this.imageBase = imageBase;
        this.executionTag = executionTag;
        this.executionNumber = executionNumber;
        this.dateOfExecution = dateOfExecution;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public String getScenarioName() {
        return scenarioName;
    }

    public void setScenarioName(String scenarioName) {
        this.scenarioName = scenarioName;
    }

    public String getComponentName() {
        return componentName;
    }

    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    public Time getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(Time executionTime) {
        this.executionTime = executionTime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getStatusTest() {
        return statusTest;
    }

    public void setStatusTest(String statusTest) {
        this.statusTest = statusTest;
    }

    public String getImageBase() {
        return imageBase;
    }

    public void setImageBase(String imageBase) {
        this.imageBase = imageBase;
    }

    public String getExecutionTag() {
        return executionTag;
    }

    public void setExecutionTag(String executionTag) {
        this.executionTag = executionTag;
    }

    public int getExecutionNumber() {
        return executionNumber;
    }

    public void setExecutionNumber(int executionNumber) {
        this.executionNumber = executionNumber;
    }

    public Date getDateOfExecution() {
        return dateOfExecution;
    }

    public void setDateOfExecution(Date dateOfExecution) {
        this.dateOfExecution = dateOfExecution;
    }
}
