package com.advice.automation.automationreport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.util.List;

@RestController
public class CucumberReportController {

    @Autowired
    private CucumberReportService cucumberReportService;

    @GetMapping("/reports")
    public List<CucumberReport> listAll(){
        return cucumberReportService.listAll();
    }

    @GetMapping("/reportsdesc")
    public List<CucumberReport> listAllDesc(){
        return cucumberReportService.findFirst50ByOrderByIdDesc();
    }

//    @GetMapping("/reports/executiontaganddate")
//    @ResponseBody
//    public List<CucumberReport> getexecdate(@RequestParam(name = "execution_tag") String execution_tag, @RequestParam(name = "date_execution") String date_execution) {
//        return cucumberReportService.listAll();
//    }

    @GetMapping("/reports/executiontag")
    @ResponseBody
    public List<CucumberReport> getexe(@RequestParam(name = "executionTag") String executionTag) {
        return cucumberReportService.findByExecutionTag(executionTag);
    }

    @GetMapping("/executiontaganddate")
    @ResponseBody
    public List<CucumberReport> getexe(@RequestParam(name = "executionTag") String executionTag,@RequestParam(name = "dateOfExecution") Date dateOfExecution) {
        return cucumberReportService.findByExecutionTagAndDateOfExecution(executionTag,dateOfExecution);
    }

    @GetMapping("/tagdatenumber")
    @ResponseBody
    public List<CucumberReport> getexe(@RequestParam(name = "executionTag") String executionTag,@RequestParam(name = "dateOfExecution") Date dateOfExecution,@RequestParam(name = "executionNumber") int executionNumber) {
        return cucumberReportService.findByExecutionTagAndDateOfExecutionAndExecutionNumber(executionTag,dateOfExecution,executionNumber);
    }

//    @GetMapping("/executiontags")
//    public List<String> getDistinctExecutionTag() {
//        return cucumberReportService.ExecutionTag();
//    }

//    @GetMapping("/reports/date")
//    @ResponseBody
//    public List<CucumberReport> getDate(@RequestParam(name = "date_execution") String date_execution) {
//        return cucumberReportService.findByExecution_tag(date_execution);
//    }
}
