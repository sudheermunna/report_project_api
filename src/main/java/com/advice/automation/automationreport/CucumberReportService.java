package com.advice.automation.automationreport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class CucumberReportService {
    @Autowired
    private CucumberReportRepository cucumberReportRepository;

    public List<CucumberReport> listAll(){
        return cucumberReportRepository.findAll();
    }

    public void save(CucumberReport cucumberReport){
        cucumberReportRepository.save(cucumberReport);
    }

    public CucumberReport get(Integer id){
        return cucumberReportRepository.findById(id).get();
    }

    public List<CucumberReport> findByExecutionTag(String executionTag){
        return cucumberReportRepository.findByExecutionTag(executionTag);
    }

    public List<CucumberReport> findByExecutionTagAndDateOfExecution(String executionTag, Date dateOfExecution){
        return cucumberReportRepository.findByExecutionTagAndDateOfExecution(executionTag,dateOfExecution);
    }

//    public List<String> ExecutionTag(){
//        return cucumberReportRepository.ExecutionTag();
//    }

    public List<CucumberReport> findByExecutionTagAndDateOfExecutionAndExecutionNumber(String executionTag, Date dateOfExecution, int executionNumber){
        return cucumberReportRepository.findByExecutionTagAndDateOfExecutionAndExecutionNumber(executionTag,dateOfExecution, executionNumber);
    }

    public List<CucumberReport> findFirst50ByOrderByIdDesc(){
        return cucumberReportRepository.findFirst50ByOrderByIdDesc();
    }

    public void delete(Integer id){
        cucumberReportRepository.deleteById(id);
    }

}
