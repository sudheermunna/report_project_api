package com.advice.automation.automationreport;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomationReportApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomationReportApplication.class, args);
	}

}
