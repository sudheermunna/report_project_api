package com.advice.automation.automationreport;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.List;

public interface CucumberReportRepository extends JpaRepository<CucumberReport,Integer> {
    List<CucumberReport> findByExecutionTag(String executionTag);
    List<CucumberReport> findByExecutionTagAndDateOfExecution(String executionTag, Date dateOfExecution);
    List<CucumberReport> findByExecutionTagAndDateOfExecutionAndExecutionNumber(String executionTag, Date dateOfExecution, int executionNumber);
    List<CucumberReport> findFirst50ByOrderByIdDesc();


//    List<CucumberReport> findBydate_execution(String date_execution);
}
